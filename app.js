var express = require('express')
    , app = express()
    , server = require('http').createServer(app)
    , io = require('socket.io').listen(server);
var $ = require('jquery');
var Db = require('mongodb').Db,
    Connection = require('mongodb').Connection,
    Server = require('mongodb').Server;

app.use(express.static('public'));
server.listen(3000);

var conn = new Db('trivia', new Server('localhost', Connection.DEFAULT_PORT, {}));

var countPosts = function (db, myCollection, query, cb) {
    db.open(function(err, db) {
		db.collection(myCollection, function(err, collection) {
		    collection.find(query).count(function(err, count) {
		    	db.close();
				return cb(err, count);
		    });
		});
    });
};

var queryOne = function (db, myCollection, query, cb) {
    // perform the {query} on the collection and invoke the nextFn when done
    db.open(function(err, db) {
		db.collection(myCollection, function(err, collection) {
		    collection.findOne(query, function(err, doc) {
		    	db.close();
				return cb(doc);
		    });
		});
    });
};

var players = [];
var stage = 0;
var interval = false;
var currentQuestion = {question: 'En svår fråga.', answer: 'Ett svårt svar', imgurl: 'ingen url'};

var shouldRun = function() {
	if(players.length > 1) {
		return true;
	}
	return false;
};

var getRandomQuestion = function(cb) {
	queryOne(conn, 'trivia', {random_point :{ '$near' : [Math.random(), 0] }}, function(doc) {
		cb(doc);
	});
};

var getHint = function(str, count) {
	var full = str;
	str = str.substring(0, count);
	for(var i = 0; i<(full.length - count); i++) {
		str = str + '*';
	}
	return str.toLowerCase();
};

var playerExist = function(id) {
	for(var i in players) {
		if(players[i].id && players[i].id == id) {
			return true;
		}
	}
	return false;
};

var usernameExist = function(username) {
	for(var i in players) {
		if(players[i].username && players[i].username == username) {
			return true;
		}
	}
	return false;
};

var changeUsername = function(id, username) {
	for(var i in players) {
		if(players[i].id == id) {
			players[i].username = username;
			return true;
		}
	}
	return false;
};

var getPlayersWithoutId = function() {
	var p = [];
	for(var i in players) {
		p.push({username: players[i].username, score: players[i].score});
	}
	return p;
}


function Game(){
	var that = this;
	this.gameLoop = function() {
		interval = setInterval(function(){
			if(stage == 0) {
				getRandomQuestion(function(item) {
					currentQuestion = item;
					io.sockets.emit('update', {type: 'question', question: currentQuestion.question, img: currentQuestion.imgurl});
					stage++;
				});
			}
			else if(stage > 0 && stage < 4) {
				io.sockets.emit('update', {type: 'hint', hint: getHint(currentQuestion.answer, stage)});
				stage++;
			}
			else {
				io.sockets.emit('update', {type: 'answer', answer: currentQuestion.answer});
				stage = 0;
			}
		}, 10000);
	};
	this.startGame = function() {
		if(!shouldRun() && typeof interval == 'object') {
			clearInterval(interval);
			interval = false;
			io.sockets.emit('update', {type: 'gameinfo', info: 'Det finns mindre än två spelare kvar, så spelet är nu avslutat.'});
		}
		else if(shouldRun() && typeof interval != 'object') {
			io.sockets.emit('update', {type: 'gameinfo', info: 'Det finns nu två spelare med och spelet börjar strax!'});
			that.gameLoop();
		}
	};

	this.addScore = function(username) {
		if(username) {
			for (var i in players) {
				if(players[i].username == username) {
					players[i].score += 1;
				}
			}
		}
		that.updatePlayerlist();
	};

	this.updatePlayerlist = function() {
		io.sockets.emit('player-update', {players: getPlayersWithoutId()});
	};

	this.guess = function(guess, username) {
		io.sockets.emit('update', {type: 'guess', guess: guess, player: username});
		if(currentQuestion.answer && guess.toLowerCase().trim() == currentQuestion.answer.toLowerCase() && guess.toLowerCase() != 'asfag123123??zp+++') {
			for (var i in players) {
				if(username && players[i].username == username) {
					io.sockets.emit('update', {type: 'correct', answer: currentQuestion.answer, player: players[i].username});
					that.addScore(username);
					currentQuestion.answer = 'asfag123123??zp+++';
				}
			}
			stage = 0;
		}
		if(guess.toLowerCase() == '!next') {
	      io.sockets.emit('update', {type: 'answer', answer: currentQuestion.answer});
	      stage = 0;
	    }

	    if(guess.toLowerCase() == '!score') {
			io.sockets.emit('update', {type: 'scorelist', scorelist: getPlayersWithoutId()});
	    }
	};
};


var game = new Game();

app.get('/', function (req, res) {
  res.sendfile('public/app.html');
});

io.sockets.on('connection', function (socket) {
	socket.on('username', function (data) {
		data.username = removeTags(data.username);
		var player = {id: socket.id, username: data.username, score: 0};

		if(data.old && playerExist(socket.id) && !usernameExist(data.username)) {
			if(changeUsername(socket.id, data.username)) {
				socket.broadcast.emit('update', {type: 'gameinfo', info: 'Spelare: ' + data.old + ' är nu känd som ' + player.username + '.'});
				game.updatePlayerlist();
			}
		}
		else {
			if(!playerExist(socket.id)) {
				players.push(player);
				game.updatePlayerlist();
				game.startGame();
			}
			else {
				socket.emit('error', {type: 'player-exists'});
			}
		}
	});

	socket.on('guess', function (data) {
		if(!data.username || data.username == '') {
			socket.emit('update', {type: 'gameinfo', info: 'Du måste ansluta dig längst ner till vänster för att vara med och spela!'});
		}
		else {
			data.guess = removeTags(data.guess);
			data.username = removeTags(data.username);
			game.guess(data.guess, data.username);
		}
	});

	socket.on('disconnect', function() {
		for(var i in players) {
			if(players[i].id == socket.id) {
				socket.broadcast.emit('update', {type: 'gameinfo', info: 'Spelare: ' + players[i].username + ' har lämnat spelet.'});
				players.splice(i, 1);
			}
		}
		io.sockets.emit('player-update', {players: players});
		game.startGame();
	});
});


var tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';

var tagOrComment = new RegExp(
    '<(?:'
    // Comment body.
    + '!--(?:(?:-*[^->])*--+|-?)'
    // Special "raw text" elements whose content should be elided.
    + '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*'
    + '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
    // Regular name
    + '|/?[a-z]'
    + tagBody
    + ')>',
    'gi');
function removeTags(html) {
  var oldHtml;
  do {
    oldHtml = html;
    html = html.replace(tagOrComment, '');
  } while (html !== oldHtml);
  return html.replace(/</g, '&lt;');
}

	
var socket;
if(window.location.href.indexOf("localhost") == -1) {
	socket = io.connect('http://192.168.1.104:3000');
}
else {
	socket = io.connect('http://localhost:3000');
}

var username = '';

$('#submit-username').click(function(event) {
	var old = username;
	if(old === '') {
		username = $('#username').val();	
		socket.emit('username', {username: username});
		$(this).text('Uppdatera');
	}
	else {
		username = $('#username').val();
		socket.emit('username', {username: username, old: old});
	}
});

$(document).on('keydown', '#input', function(e) {
	var val = $(this).val();
	if(e.which == 13) {
		socket.emit('guess', {username: username, guess: val});
		$(this).val('');
	}
});

socket.on('player-update', function(data) {
	$('#usernames').html('');
	for (var key in data.players) {
	   var player = data.players[key];
	   $('#usernames').append('<p class="user">' + player.username + ' (' + player.score + ')</p>');
	}
});

socket.on('update', function(data) {
	var el = $('#playground');
	var type = data.type;
	if(type === 'question') {
		el.append('<p class="bot"><span class="make-bold">Fråga:</span> ' + data.question + '</p>');
		if(data.img && data.img.indexOf('http') != -1) {
			el.append('<img class="bot" src="' + data.img + '" />');
		}

	}
	else if(type === 'hint') {
		el.append('<p class="bot"><span class="make-bold">Ledtråd:</span> ' + data.hint + '</p>');
	}
	else if(type === 'answer') {
		el.append('<p class="bot"><span class="make-bold">Tiden är ute, svaret var:</span> ' + data.answer + '</p>');
	}
	else if(type === 'guess') {
		el.append('<p>' + data.player + '> ' + data.guess + '</p>');
	}
	else if(type === 'correct') {
		el.append('<p class="bot"><span class="make-bold">' + data.player + ' gissade rätt! Svaret var: </span> ' + data.answer + '</p>');
		el.append('<p class="bot">Förbered er, nästa fråga kommer snart!</p>');
	}
	else if(type === 'scorelist') {
		for(var i in data.scorelist) {
			var player = data.scorelist[i];
			el.append('<p class="bot"><span class="make-bold">' + player.username + ': </span> ' + player.score + ' poäng!</p>');
		}
	}
	else if(type === 'gameinfo') {
		el.append('<p class="bot">' + data.info + '</p>');
	}
    el.animate({"scrollTop": el[0].scrollHeight}, "slow");
});

socket.on('error', function(data) {
	var type = data.type || '';
	if(type === 'player-exists') {
		alert("En spelare med samma namn finns redan, var god välj ett annat namn!");
	}
});


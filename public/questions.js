var questions = [
  {
    "Question": "Detta bilspel för Amiga heter?",
    "Anwser": "4D Sports Driving",
    "ImgUrl": "http://trivia.se/bilder/questions/100-20070723020713.jpg"
  },
  {
    "Question": "Vad heter huvudkaraktären i detta spel?",
    "Anwser": "Rick Dangerous",
    "ImgUrl": "http://trivia.se/bilder/questions/101-20070723020743.jpg"
  },
  {
    "Question": "Vad var grundvapnet i detta spel?",
    "Anwser": "En piska",
    "ImgUrl": "http://trivia.se/bilder/questions/102-20070723020740.jpg"
  },
  {
    "Question": "Vad heter detta spel?",
    "Anwser": "Excite Bike",
    "ImgUrl": "http://trivia.se/bilder/questions/103-20070723020736.jpg"
  },
  {
    "Question": "Vilket av följande är inte en boss i Megaman 1?",
    "Anwser": "Zapman",
    "ImgUrl": "http://trivia.se/bilder/questions/104-20070723020706.jpg"
  },
  {
    "Question": "Super Mario Bros släpptes...",
    "Anwser": "1985",
    "ImgUrl": "http://trivia.se/bilder/questions/105-20070723020751.jpg"
  },
  {
    "Question": "Bilden är från...",
    "Anwser": "The Curse of Monkey Island",
    "ImgUrl": "http://trivia.se/bilder/questions/106-20070723020759.jpg"
  },
  {
    "Question": "Vad heter spelet?",
    "Anwser": "Duke Nukem 3D",
    "ImgUrl": "http://trivia.se/bilder/questions/107-20070723020709.jpg"
  },
  {
    "Question": "Vad heter Spelet?",
    "Anwser": "Full Throttle",
    "ImgUrl": "http://trivia.se/bilder/questions/109-20070723020736.jpg"
  }
];